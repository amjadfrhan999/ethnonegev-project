from rest_framework import serializers
from .models import Events, Artist


class EventsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Events
        fields = ('id', 'name', 'date', 'price', 'location', 'image', 'artist')


class ArtistSerializer(serializers.ModelSerializer):
    class Meta:
        model = Artist
        fields = ('id', 'name', 'genre', 'story', 'username', 'password', 'email')
