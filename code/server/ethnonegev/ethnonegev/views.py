from rest_framework import status, viewsets, generics
from rest_framework.decorators import api_view, renderer_classes
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from .models import Artist, Events
from django.core import serializers
from .serializers import EventsSerializer


@api_view(['GET'])
@renderer_classes([JSONRenderer])
def get_events(request):
    res = serializers.serialize('json', Events.objects.all())
    return Response(res, status=status.HTTP_200_OK)


@api_view(['GET'])
@renderer_classes([JSONRenderer])
def get_all_artist(request):
    res = serializers.serialize('json', Artist.objects.all())
    return Response(res, status=status.HTTP_200_OK)


@api_view(['GET'])
@renderer_classes([JSONRenderer])
def get_artist(request, artistid):
   return Response(serializers.serialize("json", Artist.objects.filter(id=artistid)))



@api_view(['POST'])
@renderer_classes([JSONRenderer])
def add_artist(request):
    data = request.data
    new_artist = Artist(name=data['name'], genre=data['genre'], story=data['story'], username=data['username'], password=data['password'], email=data['email'])
    new_artist.save()
    return Response("added", status=status.HTTP_200_OK)


@api_view(['POST'])
@renderer_classes([JSONRenderer])
def login_artist(request):
    data = request.data
    exists = False
    if Artist.objects.filter(username=data['username'], password=data['password']).exists():
        exists = True
    if exists == True:
        artist = Artist.objects.get(username=data['username'], password=data['password'])
        return Response(artist.id, status=status.HTTP_200_OK)
    else:
        return Response(-1, status=status.HTTP_200_OK)


@api_view(['POST'])
@renderer_classes([JSONRenderer])
def add_event(request):
    data = request.data
    new_event = Events(date=data['date'], location=data['location'], artist_id=data['artist_id'], image=data['image'], name=data['name'], price=data['price'], discreption=data['discreption'])
    new_event.save()
    return Response("added", status=status.HTTP_200_OK)
