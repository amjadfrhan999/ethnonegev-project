from django.db import models


class Events(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=100, default='')
    discreption = models.CharField(max_length=1500, default='')
    date = models.DateTimeField()
    price = models.IntegerField(default='0')
    location = models.CharField(max_length=100)
    image = models.ImageField(default='')
    artist = models.ForeignKey('Artist', on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class Artist(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=100)
    genre = models.CharField(max_length=50)
    story = models.TextField(blank=True)
    username = models.CharField(max_length=100)
    password = models.CharField(max_length=20)
    email = models.EmailField(max_length=245)

    def __str__(self):
        return self.name


class Users(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=100)
    username = models.CharField(max_length=100)
    password = models.CharField(max_length=20)
    email = models.EmailField(max_length=245)

    def __str__(self):
        return self.name


class Orders(models.Model):
    id = models.AutoField(primary_key=True)
    date = models.DateTimeField()
    location = models.CharField(max_length=100)
    user = models.ForeignKey('Users', on_delete=models.CASCADE)
    event = models.ForeignKey('Events', on_delete=models.CASCADE)

    def __str__(self):
        return self.id

