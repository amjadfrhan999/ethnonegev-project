import React, {useEffect, useState} from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import 'jquery';

import service from "../service/api";
import './Table.css';


export default function BasicTable() {

    const [events, setEvents] = useState([])
    const [searchTerm, setSearchTerm] = useState('')

    useEffect(() => {
        service.EventsService.GetAllEvents().then((events) => {
            console.log('GetAllEvents',events);
            setEvents(events);
        })
    }, [])

    return (
        <div className={'the-table'}>
            <input type="text" placeholder="type artist name or location" onChange={event => {
                setSearchTerm(event.target.value)
            }}/>

            <TableContainer component={Paper}>
                <Table className={'Table'}  aria-label="simple table">
                    <TableHead>
                        <TableRow  onClick={() => console.log("artist")}>
                            <TableCell>name</TableCell>
                            <TableCell align="right">date</TableCell>
                            <TableCell align="right">location&nbsp;(g)</TableCell>
                            <TableCell align="right">image&nbsp;</TableCell>

                            <TableCell> order </TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>


                        {events.filter(value => {
                            if (searchTerm === "") {
                                return value
                            } else if (value.fields.name.toLowerCase().includes(searchTerm.toLowerCase())
                                ||
                                value.fields.location.toLowerCase().includes(searchTerm.toLowerCase())
                            ) {
                                return value
                            }
                        }).map((event) => (
                            <TableRow className='clickable-row' key={event.fields.id}>
                                <TableCell component="th" scope="row"> {event.fields.name} </TableCell>
                                <TableCell align="right">{event.fields.date}</TableCell>
                                <TableCell align="right">{event.fields.location}</TableCell>
                                <TableCell align="right">{event.fields.image}</TableCell>
                                <TableCell>
                                    <button className="float-left submit-button">order</button>
                                </TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer></div>


    );
}




