import React, {useEffect, useState} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import 'jquery';
import service from "../service/api";
import './Table.css';
import "./result.css"
import { useHistory } from 'react-router-dom';


const useStyles = makeStyles({
    table: {
        minWidth: 150,
    },
});

export default function  BasicTable(props) {
    const classes = useStyles();

    const [id, setId] = useState(1)
    const [events, setEvents] = useState([])
    const [searchTerm, setSearchTerm] = useState('')
    const data={
        location:props.location.state.location,
        date:props.location.state.date,
        price:props.location.state.price,
        experience:props.location.state.experience,
    }


    useEffect(() => {
        service.EventsService.GetAllEvents().then((events) => {
            console.log('GetAllEvents',events);
            setEvents(events);
        })
    }, [])


    let history = useHistory();
    const redirect = (name, date, location,price, discreption ,artist) => {
        history.push({
            pathname: '/Event&Artist',
            search: 'Artist-&-event',
            state: {
                name:name,
                date:date,
                location:location,
                price:price,
                artist:artist,
                discreption :discreption
            }
        });
    }


    return (
        <div >
            <div className="colorb">
            <p >
                <br/><br/><br/><br/><br/><br/>
            </p>

            <p className="left" >מופעים</p>
                <br/>
            </div>
            <ul className={'tester'}>
                        {events.filter(value => {
                            if (
                                value.fields.location.toLowerCase().includes(data.location)
                                &&(value.fields.price>=data.price[0]&&value.fields.price<=data.price[1]
                                && value.fields.date.includes(data.date)
                                )

                            ) {
                                return value
                            }
                        }).map((event) => (
                            <li key={event.fields.id}>

                            <div className={"item"}>

                                <div align="right" >{<img src={event.fields.image} width="70" height="70"/>}</div>

                                <div >
                                    {event.fields.name}

                                    { <button className="left" onClick={() => redirect(event.fields.name, event.fields.date, event.fields.location,event.fields.price,event.fields.discreption , event.fields.artist)} >הזמן</button>}

                                </div>
                                <div align="right">{event.fields.date}</div>
                                <div align="right">{event.fields.location}</div>
                                <br/><br/><br/>
                            </div>
                            </li>
                        ))}
            </ul>

                 </div>

    );
}


