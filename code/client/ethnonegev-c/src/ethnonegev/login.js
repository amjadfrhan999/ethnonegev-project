import React, {useEffect} from 'react';
import './login.css';
import service from "../service/api";


export default class login extends React.Component {
    constructor(props) {
        super(props);


        this.action = this.action.bind(this);
        this.Return = this.Return.bind(this);
        this.handleUsernameChange = this.handleUsernameChange.bind(this);
        this.handlePasswordChange = this.handlePasswordChange.bind(this);
        this.handleIdChange = this.handleIdChange.bind(this);

    }

    state = {
        username:'',
        password:'',
        id:-1
    }

    action() {
        /*
                this.props.history.push('/HomePage');
        */
        service.LoginService.login_artist(this.state.username, this.state.password).then((id) => {
            if (id==-1)
                alert('username or password is wrong');
            else {
                alert('hello user');
                this.props.history.push({
                    pathname: '/profile',
                    state:{ArtistId: id}
                });
            }
            this.handleIdChange(id);
        })
    }
    Return() {
        this.props.history.push('/HomePage')
    }
    handleUsernameChange = (event) =>{
        this.setState({username:event.target.value})
    }
    handlePasswordChange = (event) =>{
        this.setState({password:event.target.value})
    }
    handleIdChange= (event) =>{
        this.setState({id:event})
    }

    render(){

        return (

            <div className="paper">
                <div>
                    <img className={'Return'} onClick={this.Return} src={'https://www.freeiconspng.com/uploads/right-arrow-icon-22.png'}/>
                    <br/><br/><br/><br/><br/><br/><br/><br/><br/>
                    <form className="center" >
                        <img src="https://mpng.subpng.com/20180329/zue/kisspng-computer-icons-user-profile-person-5abd85306ff7f7.0592226715223698404586.jpg" alt="profile" width="100" height="100"/>
                        <br/>
                        <input id={'username'} value={this.state.username} type="text" placeholder="שם משתמש" required onChange={this.handleUsernameChange}/>
                        <br/>
                        <input id={'password'} value={this.state.password} type="password" placeholder="סיסמה" required onChange={this.handlePasswordChange}/>
                        <br/>
                    </form>
                    <button type={'submit'} className="ButtonLogIn" onClick={this.action}>login</button>
                    <br/> <br/> <br/> <br/> <br/> <br/> <br/> <br/> <br/> <br/> <br/>

                </div>
            </div>

        )
    }
}