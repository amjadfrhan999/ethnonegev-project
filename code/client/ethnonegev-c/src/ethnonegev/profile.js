import React, {useEffect, useState} from 'react';
import service from "../service/api";
import {useHistory} from "react-router-dom";


export default function Profile(props) {
    const [id, setId] = useState(props.location.state.ArtistId)
    const [events, setEvents] = useState([])

    let history = useHistory();
    const AddEvent = () => {
        history.push({
            pathname: '/AddEvent',
            search: 'AddEvent',
            state:{ArtistId: id}
        });
    }
    const home = () => {
        history.push({
            pathname: '/HomePage',
            search: 'HomePage',
        });
    }

    useEffect(() => {
        service.ArtistService.GetArtist(id).then((events) => {
            console.log('GetArtist',events);
            setEvents(events);
        })
    }, [])


    return (
        <div>

            <br/><br/>

            {events.map((event) =>(
            <h3> {event.fields.name}  שלום לך  </h3>))}

            <br/>

            {events.map((event) =>(
                <div>
                    <h3 id={'ArtistInfo'}>: פרטים שלך</h3>
                    <img className={'ArtistImg'} src={'https://upload.wikimedia.org/wikipedia/commons/thumb/4/40/Cercle_noir_100%25.svg/1024px-Cercle_noir_100%25.svg.png'}/>
                    <div className={'ArtistName'}>
                        {event.fields.name}
                    </div>
                    <div className={'ArtistDescription'}>
                        {event.fields.story}
                    </div>
                </div>
            ))}

            <br/><br/>
            <button className={'btnAddEvent'} onClick={AddEvent}>הוספת אירוע</button>
            <br/>
            <button className={'btnAddEvent'} onClick={home}>חזרה לדף הבית</button>
        </div>
    );


}