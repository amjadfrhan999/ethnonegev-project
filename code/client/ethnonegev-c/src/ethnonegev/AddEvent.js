import React from 'react';
import service from "../service/api";
import './AddEvent.css'


export default class  AddEvent extends React.Component{

    constructor(props) {
        super(props);

        this.SubmitMe = this.SubmitMe.bind(this);
        this.handleDateChange = this.handleDateChange.bind(this);
        this.handleLocationChange = this.handleLocationChange.bind(this);
        this.handleArtistIdChange = this.handleArtistIdChange.bind(this);
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handlePriceChange = this.handlePriceChange.bind(this);
        this.handleDiscreptionChange = this.handleDiscreptionChange.bind(this);

    }

    state = {
        date:'',
        location:'',
        artist_id:this.props.location.state.ArtistId,
        image:'https://www.istoreil.co.il/media/catalog/category/Cat-icons_Music_Acc.png',
        name:'',
        price:'',
        discreption:''
    }

    handleDateChange = (event) => {
        this.setState({date:event.target.value})
    }
    handleLocationChange = (event) => {
        this.setState({location:event.target.value})
    }
    handleArtistIdChange = (event) => {
        this.setState({artist_id:event.target.value})
    }
    handleNameChange = (event) => {
        this.setState({name:event.target.value})
    }
    handlePriceChange = (event) => {
        this.setState({price:event.target.value})
    }
    handleDiscreptionChange = (event) => {
        this.setState({discreption:event.target.value})
    }


    SubmitMe = () => {
        if(this.state.name !=='' && this.state.date !=='' && this.state.discreption !=='' && this.state.price !=='' && this.state.price > -1 && this.state.artist_id !=='' && this.state.location !=='')
        service.AddEventService.addEvent(this.state.date, this.state.location, this.state.artist_id, this.state.image, this.state.name, this.state.price, this.state.discreption)
            .then(response => {
                if(response === 'added'){
                    alert("Event added successfully");
                }
                else {
                    alert("not working");
                }
                console.log(response);
            })
        else{
            console.log('sorry')
        }
        this.props.history.push({
            pathname: '/profile',
            state:{ArtistId: this.state.artist_id}
        });
    }



    render() {
        return (
            <div>
                <h1>הוספת אירוע</h1>
                <form>
                    <input id={'name'} value={this.state.name} type="text" placeholder="שם" required onChange={this.handleNameChange}/>
                    <input id={'location'} value={this.state.location} type="text" placeholder="מיקום" required onChange={this.handleLocationChange}/>
                    <input id={'date'} value={this.state.date} type="date"  required onChange={this.handleDateChange}/>
                    <input id={'price'} value={this.state.price} type="number" placeholder="מחיר" required onChange={this.handlePriceChange}/>
                    <input id={'discreption'} value={this.state.discreption} type="text" placeholder="תיאור" required onChange={this.handleDiscreptionChange}/>
                </form>
                <br/>
                <button className={'btnAddEvent'} onClick={this.SubmitMe}> הוספה</button>
            </div>
        );
    }
}