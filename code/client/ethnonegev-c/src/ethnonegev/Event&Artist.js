import React, {useState} from 'react';
import Event from "./Event";
import Artist from "./Artist";


class EventArtist extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: this.props.location.state.name,
            date: this.props.location.state.date,
            price: this.props.location.state.price,
            location: this.props.location.state.location,
            image: '',
            discreption:this.props.location.state.discreption,
            artist: this.props.location.state.artist,

        }
    }

    render() {
        return (
            <div>

                <Event EventId={this.state.id} EventName={this.state.name}
                       EventDate={this.state.date} EventLocation={this.state.location}
                       EventPrice={this.state.price} EventImage={this.state.image}
                       EvenDescription={this.state.discreption}/>
                <br/>
                <div align={'center'}>--------------------------------------------</div>
                <Artist ArtistId={this.state.artist}/>
            </div>
        );
    }
}
export default EventArtist
