import React, {useEffect, useState} from 'react';
import './Artist.css'
import service from "../service/api";


export default function Artist(props) {
    const [events, setEvents] = useState([])

    useEffect(() => {
        let id = props.ArtistId;
        service.ArtistService.GetArtist(id).then((events) => {
            console.log('GetArtist',events);
            setEvents(events);
        })
    }, [])

    return (
        <div>
            {events.map((event) =>(
                <div>
                    <h3 id={'ArtistInfo'}>על האמן</h3>
                    <img className={'ArtistImg'} src={'https://upload.wikimedia.org/wikipedia/commons/thumb/4/40/Cercle_noir_100%25.svg/1024px-Cercle_noir_100%25.svg.png'}/>
                    <div className={'ArtistName'}>
                        {event.fields.name}
                    </div>
                    <div className={'ArtistDescription'}>
                        {event.fields.story}
                    </div>
                </div>
            ))}
        </div>
    );
}
