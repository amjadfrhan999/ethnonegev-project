import React, {useState} from 'react';
import service from "../service/api";


export default class  Register extends React.Component{

    constructor(props) {
        super(props);
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleGenreChange = this.handleGenreChange.bind(this);
        this.handleStoryChange = this.handleStoryChange.bind(this);
        this.handleUsernameChange = this.handleUsernameChange.bind(this);
        this.handlePasswordChange = this.handlePasswordChange.bind(this);
        this.handleEmailChange = this.handleEmailChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);

    }

    state = {
        name:'',
        genre:'',
        story:'',
        username:'',
        password:'',
        email:''
    }


    onSubmit = () => {
        if(this.state.name !== '' && this.state.genre !== '' && this.state.story !== '' && this.state.username !== '' && this.state.password !== '' && this.state.email !== '' ) {
            service.AddArtistService.addArtist(this.state.name, this.state.genre, this.state.story, this.state.username, this.state.password, this.state.email)
                .then(response => {
                    if (response === 'added') {
                        alert("Artist added successfully");
                    }
                    console.log(response);
                })
            this.props.history.push('/HomePage');
        }
        else {
            alert('error')
        }
    }


    handleNameChange = (event) =>{
        this.setState({name:event.target.value})
    }
    handleGenreChange = (event) =>{
        this.setState({genre:event.target.value})
    }
    handleStoryChange = (event) =>{
        this.setState({story:event.target.value})
    }
    handleUsernameChange = (event) =>{
        this.setState({username:event.target.value})
    }
    handlePasswordChange = (event) =>{
        this.setState({password:event.target.value})
    }
    handleEmailChange = (event) =>{
        this.setState({email:event.target.value})
    }

    render() {

        return (
            <div>
                <h1>הרשמה</h1>
                <form>
                    <input id={'name'} value={this.state.name} type="text" placeholder="שם" required onChange={this.handleNameChange}/>

                    <input id={'genre'} value={this.state.genre} type="text" placeholder="סגנון מוזיקה" required onChange={this.handleGenreChange}/>

                    <input id={'story'} value={this.state.story} type="text" placeholder=" חיים סיפור" required onChange={this.handleStoryChange}/>

                    <input id={'username'} value={this.state.username} type="text" placeholder="שם משתמש" required onChange={this.handleUsernameChange}/>

                    <input id={'password'} value={this.state.password} type="password" placeholder="סיסמה" required onChange={this.handlePasswordChange}/>

                    <input id={'email'} value={this.state.email} type="email" placeholder="מייל" required onChange={this.handleEmailChange}/>

                </form>
                <br/>
                <button type={"submit"} onClick={this.onSubmit} className={'btnAddEvent'}>שליחה</button>
            </div>
        );
    }
}