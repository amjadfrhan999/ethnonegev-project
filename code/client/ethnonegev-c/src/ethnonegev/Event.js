import React, {useState} from 'react';
import "./event.css"
import {useHistory} from "react-router-dom";


export default function Event(props) {

    let history = useHistory();
    const Order = () => {
        history.push({
            pathname: '/Order',
            search: 'order',
            state:{NameEvent:props.EventName,PriceEvent:props.EventPrice}
        });
    }

    return (
        <div>

            <div className="emptytop">
                <br/><br/><br/><br/><br/>
                <p className="lines">---</p>
            </div>

            <p className="name">{' '+props.EventName}</p>
            <p className="description">{props.EventDiscreption}</p>

            <p> מיקום:{' '+props.EventLocation} </p>
            <p> כרטיס ליחיד:{' '+props.EventPrice} </p>

            <p className="date">{' '+props.EventDate}<br/></p>
            <p>{' '+props.EvenDescription}</p>


            <p className="order"> <button className="order" onClick={Order}>הזמן אירוע פרטי</button> </p>

            <br/>


        </div>
    );
}