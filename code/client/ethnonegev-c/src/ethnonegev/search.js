import React, {createContext, useContext} from 'react';
import './search.css';
import Typography from "@material-ui/core/Typography";
import Slider from "@material-ui/core/Slider";
window.p = [20, 50];

function ChooseExperience(num) {
    for (let i = 1; i <= 6; i++) {
       document.getElementById('btn' + i).style.backgroundColor = 'gainsboro';
    }
   document.getElementById('btn'+num).style.backgroundColor = 'gray';
}

//--------------------------------------------------------------------------------------------
function valuetext(price) {
    return `${price}`;
}

function RangeSlider() {
    const [price, setValue] = React.useState([20, 50]);

    const handleChange2 = (event, newValue) => {
        window.p = newValue;
        setValue(newValue);
    };


    return (
        <div className={'slider'}>
            <Typography id="range-slider" gutterBottom>
                טווח מחיר
            </Typography>
            <Slider style={{color: 'gray', width: '85%'}}
                    min={0}
                    max={250}
                    value={price}
                    onChange={handleChange2}
                    valueLabelDisplay="auto"
                    aria-labelledby="range-slider"
                    getAriaValueText={valuetext}
            />
            <div className={'RangeValue'}
                 style={{textAlign: 'center'}}>{valuetext(price[0])} - {valuetext(price[1])}</div>
        </div>
    );
}

//--------------------------------------------------------------------------------------------


export default class search extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            location: '',
            date: '',
            price: [20, 50],
            experience: 'מוזיקה',
            Lectures: "הרצאות",
            attractions: "אטרקציות",
            Food: "אוכל",
            recreation: "נופש",
            Music: "מוזיקה",
            spirit: "רוח ונפש",


        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.Return = this.Return.bind(this);
    }

//------------------------- handels ------------------------------------------------
    handleChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        });
    }

    handleSubmit = (event) => {

        this.state.price = window.p;
        alert('A data was submitted: ' + this.state.location + "," + this.state.date + "," + this.state.experience + "," + this.state.price);
        this.props.history.push({
            pathname: '/result',
            search: 'Results',
            state:{location: this.state.location, date: this.state.date, experience: this.state.experience, price: this.state.price}
        });
        event.preventDefault();
    }
    Return() {
        this.props.history.push('/HomePage')
    }

//-------------------------------------------------------------------------------------
    render() {

        return (
                <div className={'search'}>
                    <img className={'Return'} onClick={this.Return} src={'https://www.freeiconspng.com/uploads/right-arrow-icon-22.png'}/>
                    <div className={'ExperienceSelection'}>
                        <span>בחירה סוג חוויה </span>
                        <div>
                            <br/>
                            <button id={'btn1'} onClick={() => ChooseExperience(1)}>הרצאות</button>
                            <button id={'btn2'} onClick={() => ChooseExperience(2)}>אטרקציות</button>
                            <button id={'btn3'} onClick={() => ChooseExperience(3)}>אוכל</button>
                            <button id={'btn4'} onClick={() => ChooseExperience(4)}>נופש</button>
                            <button id={'btn5'} onClick={() => ChooseExperience(5)}>מוזיקה</button>
                            <button id={'btn6'} onClick={() => ChooseExperience(6)}>רוח ונפש</button>
                        </div>
                    </div>
                    <br/>

                    <form onSubmit={this.handleSubmit}>

                        <div className={'PlaceFilter'}>
                            <label>מיקום
                                <div>
                                    <input type={'text'} name={'location'} placeholder={'תל אביב'}
                                           value={this.state.location} onChange={this.handleChange}/>
                                </div>
                            </label>
                        </div>
                        <br/>

                        <div className={'DateFilter'}>
                            <label>תאריך</label>
                            <div>
                                <input type={'date'} name={'date'} value={this.state.date}
                                       onChange={this.handleChange}/>
                            </div>
                        </div>
                        <br/>

                        <div>
                            <RangeSlider/>
                        </div>

                    </form>
                    <br/>
                    <div className={'SearchButton'}>
                        <input id={'SearchBtn'} type={"submit"} value={'חפש'} onClick={this.handleSubmit}/>
                    </div>

                </div>
                )
    };

}
