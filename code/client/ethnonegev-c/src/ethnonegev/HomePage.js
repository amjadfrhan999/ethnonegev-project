import React from "react";
import './HomePage.css'


class HomePage extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            experience: 'מוזיקה',
            Lectures: "הרצאות",
            attractions: "אטרקציות",
            Food: "אוכל",
            recreation: "נופש",
            Music: "מוזיקה",
            spirit: "רוח ונפש",
        };
        this.selectExp = this.selectExp.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleSubmit2 = this.handleSubmit2.bind(this);
        this.handleRegister = this.handleRegister.bind(this);
    }
    //-------------------------------------------------------------------------//
    handleSubmit = (event) => {
        this.props.history.push({
            pathname: '/search',
            search: 'search',
        });
    }
    handleSubmit2 = (event) => {
        if(event=='מוזיקה') {
            this.props.history.push({
                pathname: '/result',
                search: 'Results',
                state: {location: '', date: '', experience: event, price: [0, 250]}
            });
        }else alert('there is no '+event+' yet')
    }

    selectExp = (event) => {
        console.log('goning to search a: '+ event)
        this.handleSubmit2(event);
    }

    Login = (event) => {
        this.props.history.push({
            pathname: '/login',
            search: 'Login',
        });
    }

    handleRegister = () => {
        this.props.history.push({
            pathname: '/Register',
            search: 'Register',
        });
    }

    render(){
        return (
            <div>
                <img className={'LogIn'} onClick={this.Login} src={'https://img.favpng.com/8/18/20/computer-icons-user-profile-clip-art-png-favpng-AEn2Lu7vbd1dvVU3EG8uk8d1N.jpg'}/>
                <br/>
                <div className={'NameProj'}>EthnoNegev</div>
                <br/><br/>
                <input className={'SearchBtn'}  defaultValue={'חיפוש חוויה'} onClick={this.handleSubmit}/>
                <br/><br/>
                <div className={'ExperienceSearch'}>
                    <button id={'btn1'} onClick={() => this.selectExp(this.state.Lectures)}>הרצאות</button>
                    <button id={'btn2'} onClick={() => this.selectExp(this.state.attractions)}>אטרקציות</button>
                    <button id={'btn3'} onClick={() => this.selectExp(this.state.Food)}>אוכל</button>
                    <button id={'btn4'} onClick={() => this.selectExp(this.state.recreation)}>נופש</button>
                    <button id={'btn5'} onClick={() => this.selectExp(this.state.Music)}>מוזיקה</button>
                    <button id={'btn6'} onClick={() => this.selectExp(this.state.spirit)}>רוח ונפש</button>
                </div>
                <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
                <div className={'RegisterArtist'}>
                    <button onClick={this.handleRegister}>הרשמה כאומן</button>
                </div>


            </div>
        );
    }
}
export default HomePage