import React from "react";
import './App.css';

import search from "./ethnonegev/search";
import login from "./ethnonegev/login";
import result from "./ethnonegev/result";
import EventArtist from "./ethnonegev/Event&Artist";
import HomePage from "./ethnonegev/HomePage";
import Order from "./ethnonegev/Order"
import Register from "./ethnonegev/Register"
import AddEvent from "./ethnonegev/AddEvent"
import Profile from "./ethnonegev/profile";


import {BrowserRouter as Router, Redirect, Route, Switch} from "react-router-dom";




export default class App extends React.Component {
    render() {
        return (
            <Router>
                <div>

                    <Switch>
                            <Route path='/AddEvent' component={AddEvent}/>
                        <Route path='/Register' component={Register}/>
                        <Route path='/Order' component={Order}/>
                        <Route path='/Profile' component={Profile}/>
                        <Route path='/HomePage' component={HomePage}/>
                        <Route path='/Event&Artist' component={EventArtist}/>
                        <Route path='/result' component={result}/>
                        <Route path='/login' component={login}/>
                        <Route path="/search" component={search}/>
                        <Route exact path="/"><Redirect to="/HomePage"/>
                        </Route>
                    </Switch>


                </div>
            </Router>
        );
    }
}

