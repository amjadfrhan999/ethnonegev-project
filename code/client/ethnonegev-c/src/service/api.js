import Axios from 'axios'
import React from "react";



const $axios = Axios.create({
    baseURL: '/api/',
    headers: {
        'Content-Type': 'application/json'
    }
})


$axios.interceptors.response.use(
    (response) => response,
    (error) => {
        console.error("got error")
        console.error(error)
        throw error;
    });


class EventsService {
    static GetAllEvents() {
        return $axios
            .get('allevents',)
            .then(response => JSON.parse(response.data));
    }

}

class ArtistService {
    static GetArtist(id) {
        return $axios
            .get('artist/get/'+id,)
            .then(response => JSON.parse(response.data));
    }

}

class AddArtistService {
    static addArtist(name, genre, story, username, password, email) {
        return $axios
            .post('artist/add', {name, genre, story, username, password, email})
            .then(response => response.data)
    }
}

class LoginService {
    static login_artist(username, password) {
        return $axios
            .post('artist/login', {username, password})
            .then(response => response.data)
    }
}

class AddEventService {
    static addEvent(date, location, artist_id, image, name, price, discreption) {
        return $axios
            .post('event/add', {date, location, artist_id, image, name, price, discreption})
            .then(response => response.data)
    }
}

const service = {
    EventsService,
    ArtistService,
    AddArtistService,
    LoginService,
    AddEventService
}

export default service;

