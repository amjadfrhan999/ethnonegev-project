CREATE DATABASE IF NOT EXISTS ethnonegev;

DROP USER 'ethnonegev_admin'@'%';
-- password MD5('ethnonegev_admin')
CREATE USER 'ethnonegev_admin'@'%' IDENTIFIED WITH mysql_native_password BY '34d5c41a0f42a45ee55e696fe76aef5d';
GRANT ALL ON ethnonegev.* TO 'ethnonegev_admin'@'%';
FLUSH PRIVILEGES;
